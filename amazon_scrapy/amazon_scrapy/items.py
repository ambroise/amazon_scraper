# -*- coding: utf-8 -*-

from scrapy_djangoitem import DjangoItem
from core.models import AmazonAppModel


class AmazonScrapyItem(DjangoItem):
    # Using scrapy-djangoitem library we bind our Scrapy Item model to the one created in our models.py.
    django_model = AmazonAppModel
