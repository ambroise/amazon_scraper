# -*- coding: utf-8 -*-
import scrapy
from amazon_scrapy.items import AmazonScrapyItem
from bs4 import BeautifulSoup


class AmazonSpider(scrapy.Spider):
    """
    This is a simplified use of Scrapy spiders.
    We don't need pipelines for the basic purpose of populating Django ORM with one crawled item.
    Consequently, we place the logic directly inside the parse method.
    """

    name = 'amazon'

    # For robustness purposes we don't allow foreign amazon domains,
    # there might be discrepancies between the page layouts
    allowed_domains = ['amazon.com', 'localhost']

    # Here we declare xpath variables for each of the required info. We created them by using a browser dev tool.
    XPATH_APP_NAME = '//*[@id="mas-title"]/div/span/text()'
    XPATH_APP_VERSION = '//*[@id="masTechnicalDetails-btf"]/div[2]/span[2]/text()'
    XPATH_CHANGELOG = '//*[@id="mas-latest-updates"]//text()'
    XPATH_RELEASE_DATE = '//*[@id="productDetailsTable"]/tr/td/div/ul/li[3]'

    def __init__(self, *args, **kwargs):
        # We override the __init__ method to populate the spider with arguments gotten from the crawl method in views.py
        self.url = kwargs.get('url')
        self.domain = kwargs.get('domain')
        self.start_urls = [self.url]
        self.unique_id = kwargs.get('unique_id')

        super(AmazonSpider, self).__init__(*args, **kwargs)

    @staticmethod
    def clean_raw_html(raw_html):

        unwanted_characters = ["  ", "[", "]", r"\\n", r"\n"]

        clean_html = BeautifulSoup(raw_html, 'lxml').text

        for char in unwanted_characters:

            clean_html = clean_html.replace(char, '')

        return clean_html

    def parse(self, response):
        # We use the scrapy django-item declared inside items.py.
        item = AmazonScrapyItem()

        app_name = response.xpath(self.XPATH_APP_NAME).extract()
        app_version = response.xpath(self.XPATH_APP_VERSION).extract()
        changelog = response.xpath(self.XPATH_CHANGELOG).extract()
        del changelog[0]
        release_date = response.xpath(self.XPATH_RELEASE_DATE).extract()

        app_name = ' '.join(app_name)
        app_version = ' '.join(app_version)
        changelog = ' '.join(changelog)
        release_date = ' '.join(release_date)

        clean_app_name = self.clean_raw_html(app_name)
        clean_app_version = self.clean_raw_html(str(app_version))
        clean_changelog = self.clean_raw_html(str(changelog))
        clean_release_date = self.clean_raw_html(str(release_date))

        unique_id = self.unique_id

        # Populating the item's fields
        # This part would go in a scrapy item pipeline if the project was more complex (i.e multiple crawlers / items)
        item['app_name'] = clean_app_name
        item['release_date'] = clean_release_date
        item['app_version'] = clean_app_version
        item['changelog'] = clean_changelog
        item['unique_id'] = unique_id

        item.save()

        yield item
