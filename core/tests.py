from django.test import TestCase
from scrapyd_api import ScrapydAPI
from urllib.parse import urlparse
import time
from core.models import AmazonAppModel


class ViewCrawlTestCase(TestCase):
    def setUp(self):
        pass

    def test_scrapy(self):
        # connect scrapyd service
        scrapyd = ScrapydAPI('http://localhost:6800')

        unique_id = 'unique_id_test'
        url = 'http://127.0.0.1:8000/facebook_test'
        domain = urlparse(url).netloc

        task = scrapyd.schedule('default', 'amazon',
                                unique_id=unique_id, url=url, domain=domain)

        self.assertIsNotNone(task)

        time.sleep(10)

        # This test doesn't run because the production database is used by the spider.
        # I stumbled upon this caveat quite late in the project and didn't yet find a solution.

        # self.assertTrue(AmazonAppModel.objects.filter(unique_id=unique_id).exists())





