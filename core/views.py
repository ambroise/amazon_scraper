from uuid import uuid4
from urllib.parse import urlparse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from scrapyd_api import ScrapydAPI
from core.models import AmazonAppModel
from django.shortcuts import render


@csrf_exempt
def index(request):
    return render(request, 'index.html')


@csrf_exempt
def app_info(request):
    return render(request, 'app_info.html')


@csrf_exempt
def facebook_test(request):
    return render(request, 'facebook_test_page.html')


# connect scrapyd service
scrapyd = ScrapydAPI('http://localhost:6800')


def is_valid_url(url):
    """
    Method used to validate the url argument passed through POST request later on.
    :param url:
    :return bool:
    """
    validate = URLValidator()
    try:
        validate(url)
    except ValidationError:
        return False

    return True


@csrf_exempt
@require_http_methods(['POST', 'GET'])  # only get and post
def crawl(request):
    """
    Main method where we take a request, start Scrapy tasks and send back crawled data as json.
    :param request:
    :return JsonResponse:
    """
    # If POST is used, we expect an url as argument and use it to start a crawling task.
    if request.method == 'POST':

        url = request.POST.get('url', None)

        if not url:
            return JsonResponse({'error': 'Missing  args'})

        if not is_valid_url(url):
            return JsonResponse({'error': 'URL is invalid'})

        domain = urlparse(url).netloc  # parse the url and extract the domain
        unique_id = str(uuid4())  # create a unique ID used as link during the execution of the spider.

        # We use scrapyd wrapper to schedule a new crawling task through scrapyd API.
        # The spider's name is passed as argument.
        task = scrapyd.schedule('default', 'amazon',
                                unique_id=unique_id, url=url, domain=domain)

        return JsonResponse({'task_id': task, 'unique_id': unique_id, 'status': 'started'})

    # If GET request is used, we expect a unique_id and task_id for fetching and sending back crawled data from database
    elif request.method == 'GET':

        task_id = request.GET.get('task_id', None)
        unique_id = request.GET.get('unique_id', None)

        if not task_id or not unique_id:
            return JsonResponse({'error': 'Missing args'})

        # Checking job status through scrapyd API.
        # Possible results are -> pending, running, finished
        status = scrapyd.job_status('default', task_id)

        if status == 'finished':
            try:
                # this is the unique_id that was meant as link between task creation and sending back the crawled data.
                item = AmazonAppModel.objects.get(unique_id=unique_id)

                return JsonResponse({'unique_id': item.unique_id,
                                     'app_name': item.app_name,
                                     'status': status,
                                     'app_version': item.app_version,
                                     'changelog': item.changelog,
                                     'release_date': item.release_date,
                                     })
            except Exception as e:
                return JsonResponse({'error': str(e)})
        else:
            return JsonResponse({'status': status})
