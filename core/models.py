from django.db import models


class AmazonAppModel(models.Model):
    """
    Model class for our scraped Amazon store application. Logic goes into the the scrapy app.
    """
    unique_id = models.CharField(max_length=255, blank=True)
    url = models.CharField(max_length=255, blank=True)
    app_name = models.CharField(max_length=255, blank=True)
    app_version = models.CharField(max_length=255, blank=True)
    changelog = models.CharField(max_length=1000, blank=True)
    release_date = models.CharField(max_length=255, blank=True)
