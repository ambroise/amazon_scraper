#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='amazon_scraper',
      version='1.0',
      packages=find_packages())
