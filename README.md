# Data Theorem Python Exercise

## Directives

Build a functional web application:

* Using Python and any framework/library you are comfortable with to implement the server-side code.
* Using HTML, JavaScript and/or any framework/library you are comfortable with to implement the client-side code.

The web application's functionality is to retrieve information about a specific Android application available on the 
Amazon App Store and display it to the user.

## Main libraries used

* Django
* Scrapy
* Javascript (vanilla)

## Notes

* I encountered difficulties when deploying the project to Heroku, thus had to switch to DO instead but didn't have time
 to properly configure everything (mainly the proxy, I'll do it asap but it won't be in time for the 7 days delay since 
 I had less time than anticipated to work on it, sorry about that)
* Same thing about the way to properly test Scrapy while running it alongside Django. I didn't find a workaround in time 
to prevent the production database being used instead of the test one, thus making the test fail (commented that part 
out)

## Usage

Visit the webpage at http://104.248.241.74:8000/ and submit an amazon.com URL pointing to an app.
